"""

This script will keep the archive folder cleaned up.

The general flow for backups should be this:

1. Batch script copies user folder to repository.
2. Laozi begins archiving
3. Once a week, run the flow:
    Create archives for un-archived folders
    Move unzipped folders that have an archived copy to "Holding"
    Move folders in "Holding" that are over 30 days old to "Delete"
    Delete folders in "Delete" that are older than 30 days

Script is a work in progress.
"""

from sys import platform

import folderops
import jsonops

# import time
# import zlib
# import zipfile
# import sys

servPath = ''
holdingPath = ''
deletePath = ''
archivePath = ''


def firstrun():
    """Run configuration function, if conf file is empty."""
    confFile = 'data/conf.json'
    beenRun = jsonops.getarg(confFile, "firstrun")

    if beenRun is True:
        jsonops.updatejson(confFile, "firstrun", "false")
        return
    else:
        return


def oscheck():
    """Check OS function for determining file operation methods."""
    argsfile = "data/conf.json"

    if platform == "linux" or platform == "linux2":
        jsonops.updatejson(argsfile, "os", "linux")
    elif platform == "darwin":
        jsonops.updatejson(argsfile, "os", "macos")
    elif platform == "win32":
        jsonops.updatejson(argsfile, "os", "windows")


def createargsjson(serv, holding, delete, archive):
    """Create JSON file with path args."""
    if firstrun() is True:
        argsfile = "data/conf.json"

        jsonops.updatejson(argsfile, "serverpath", serv)
        jsonops.updatejson(argsfile, "holdingpath", holding)
        jsonops.updatejson(argsfile, "deletepath", delete)
        jsonops.updatejson(argsfile, "archivepath", archive)
        createpaths(serv, holding, delete, archive)
        jsonops.updatejson('data/conf.json', "firstrun", "False")
        return
    else:
        folderRoutine()


def createpaths(serv, holding, delete, archive):
    """Create paths to be used throughout script."""
    global servPath
    global holdingPath
    global deletePath
    global archivePath

    servPath = jsonops.getarg("data/args.json", serv)
    holdingPath = servPath + jsonops.getarg("data/args.json", holding)
    deletePath = servPath + jsonops.getarg("data/args.json", delete)
    archivePath = servPath + jsonops.getarg("data/args.json", archive)

    print("Holding: " + holdingPath)
    print("Delete: " + deletePath)
    print("Archive: " + archivePath)
    oscheck()


def folderRoutine():
    """Run through operations."""
    global servPath
    global holdingPath
    global deletePath
    global archivePath
    folderops.createZip(servPath, holdingPath, deletePath, archivePath)

    # Check the Delete folder first
    folderops.delete_file(deletePath)

    # Check the Holding folder next
    folderops.move_file(holdingPath, deletePath)


def main():
    """Run other functions with the input path."""
    global servPath
    global holdingPath
    global deletePath
    global archivePath

    if firstrun() is True:
        serv = input("Please input the root of the server path: ")
        holding = input("Please input the path to the holding folder: ")
        delete = input("Please input the path to the delete folder: ")
        archive = input("Please input the path to the archive folder: ")
        createargsjson(serv, holding, delete, archive)

        if firstrun() is False:
            folderRoutine()

    else:
        print("Configuration file not found, creating now...")
        return


main()
