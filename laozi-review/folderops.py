"""Folder operations module.

Options will be chosen based on user's OS.
Archive format can be specified in conf.json.
"""

import datetime
import os
import shutil
import zipfile
from distutils.dir_util import copy_tree

import jsonops


def createZip(serv, holding, delete, archive):
    """Import filepath from args.json, create ZIP there."""
    argsFile = 'data/args.json'
    jsonops.readjson(argsFile)  # Read JSON file.
    # Create path to directories that will be archived.
    serverPath = serv
    archpath = serverPath + archive
    archname = 'test.zip'
    fullpath = archpath + archname
    testzip = zipfile.ZipFile(fullpath, 'w')

    for folder, subfolders, files in os.walk(archpath):
        for file in files:
            testzip.write(os.path.join(folder, file), os.path.relpath(
                os.path.join(folder, file),
                archpath), compress_type=zipfile.ZIP_DEFLATED)

        testzip.close


def delete_file(delpath):
    """delete_file deletes ever file/directory of a certain age (cutoff)."""
    # Change this variable to delete files older than set:
    # days, minutes, seconds, hours, etc)
    # cutoff = datetime.timedelta(days=30)

    cutoff = datetime.timedelta(seconds=30)

    for dirpath, dirnames, filenames in os.walk(delpath):
        for file in filenames:
            curpath = os.path.join(dirpath, file)
            file_modified = datetime.datetime.fromtimestamp(
                os.path.getmtime(curpath))
            if datetime.datetime.now() - file_modified > cutoff:
                os.remove(curpath)

        for dir in dirnames:
            curpath = os.path.join(dirpath, dir)
            file_modified = datetime.datetime.fromtimestamp(os.path.getmtime(
                curpath))
            if datetime.datetime.now() - file_modified > cutoff:
                shutil.rmtree(curpath)


def move_file(path, deletePath):
    """Move files to the deletePath for the delete_file function."""
    # >>>[[[MAKE THE FUNCTION CHECK FOR ZIP EXTENSION!!]]]<<<
    # cutoff = datetime.timedelta(days=30)
    cutoff = datetime.timedelta(seconds=30)
    archivefile = "archive" or "Archive"
    movefile = "Move" or "move"
    deletefile = "Delete" or "delete"

    for dirpath, dirnames, filenames in os.walk(path):
        for file in filenames:
            if (archivefile or movefile or deletefile) not in filenames:
                curpath = os.path.join(dirpath, file)
                file_modified = datetime.datetime.fromtimestamp(
                    os.path.getmtime(curpath))
                if datetime.datetime.now() - file_modified > cutoff:
                    copy_tree(path, deletePath)
                    delete_file(path)

        for dir in dirnames:
            if (archivefile or movefile or deletefile) not in dirnames:
                curpath = os.path.join(dirpath, dir)
                file_modified = datetime.datetime.fromtimestamp(
                    os.path.getmtime(curpath))
                if datetime.datetime.now() - file_modified > cutoff:
                    full_path = os.path.join(path, dir)
                    print(full_path)
                    shutil.move(full_path, deletePath)
