"""JSON file operations.

Functions for working with JSON files.
"""

import json


def readjson(file):
    """Read data from JSON file."""
    data = []  # Variable to store read JSON data.
    with open(file, encoding="utf-8") as jfile:
        data = json.loads(jfile.read())  # Write JSON data to variable.

    return data


def writejson(file, data):
    """Write new configuration to JSON."""
    with open(file, 'w') as wjfile:  # Open file for writing.
        json.dump(data, wjfile)  # Write 'data' to JSON file.


def getarg(file, arg):
    """Find an argument in JSON file."""
    with open(file, encoding="utf-8") as jfile:
        data = json.loads(jfile.read())  # Return data in JSON file.
        data.get(arg)  # Return argument if found in JSON file.


def updatejson(file, arg, newArg):
    """Update value in a JSON file."""
    with open(file) as infile:
        data = json.load(infile)
    data[arg] = newArg
    with open(file, 'w') as outfile:
        json.dump(data, outfile)
